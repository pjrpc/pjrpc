module without-code-gen

go 1.22

toolchain go1.22.0

require gitlab.com/pjrpc/pjrpc/v2 v2.0.0

replace gitlab.com/pjrpc/pjrpc/v2 => ../../
