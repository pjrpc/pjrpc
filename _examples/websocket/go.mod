module websocket

go 1.22

toolchain go1.22.0

replace (
	gitlab.com/pjrpc/pjrpc/v2 => ../../
	gitlab.com/pjrpc/pjrpc/ws => ../../ws
)

require (
	gitlab.com/pjrpc/pjrpc/v2 v2.5.1
	gitlab.com/pjrpc/pjrpc/ws v1.0.0
)

require nhooyr.io/websocket v1.8.10
