package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
	"gitlab.com/pjrpc/pjrpc/ws"

	"websocket/model"
	"websocket/model/rpcclient"
	"websocket/model/rpcserver"
)

func main() {
	svc := &service{pool: make(map[net.Conn]rpcclient.NotificatorServerClient)}

	wsConf := &ws.Config{
		Accept: accept,
	}

	wsServ := ws.NewServerWebsocket(wsConf)

	wsServ.OnNewConnection = func(_ context.Context, conn net.Conn) (accept bool) {
		log.Println("new connect:", conn.RemoteAddr())
		svc.addClient(conn)
		return true
	}

	wsServ.OnCloseConnection = func(_ context.Context, conn net.Conn) {
		log.Println("close connect:", conn.RemoteAddr())
		svc.removeClient(conn)
	}

	wsServ.OnErrorReceive = func(_ context.Context, conn net.Conn, err error) (needClose bool) {
		log.Println("error in receive:", conn.RemoteAddr(), err.Error())
		return true
	}

	wsServ.OnErrorSend = func(_ context.Context, conn net.Conn, err error) (needClose bool) {
		log.Println("error in send:", conn.RemoteAddr(), err.Error())
		return true
	}

	rpcserver.RegisterClassicServerServer(wsServ, svc)

	srv := http.NewServeMux()
	srv.Handle("/ws", wsServ)

	log.Println("started at: 8081")

	err := http.ListenAndServe("127.0.0.1:8081", srv)
	if err != nil {
		log.Fatalln("http.ListenAndServe:", err)
	}
}

func accept(w http.ResponseWriter, req *http.Request) bool {
	log.Println("accept:", req.RemoteAddr)
	return true
}

type service struct {
	mu   sync.RWMutex
	pool map[net.Conn]rpcclient.NotificatorServerClient
}

func (s *service) addClient(conn net.Conn) {
	cl := rpcclient.NewNotificatorServerClient(client.NewAsyncClient(conn))

	s.mu.Lock()
	s.pool[conn] = cl
	s.mu.Unlock()
}

func (s *service) removeClient(conn net.Conn) {
	s.mu.Lock()
	delete(s.pool, conn)
	s.mu.Unlock()
}

// Hello sends Notification to all our clients except the client of this request.
func (s *service) Hello(ctx context.Context, in *model.HelloReqResp) (*model.HelloReqResp, error) {
	conn, _ := pjrpc.ContextGetConnection(ctx)

	remoteAddr := conn.RemoteAddr()

	log.Println("got message from:", in.Name, remoteAddr)

	notify := &model.NotifyReq{
		Message: fmt.Sprintf("I have got 'hello' request from: %s (%s)", in.Name, remoteAddr),
	}

	s.mu.RLock()

	for c, cl := range s.pool {
		if conn == c {
			continue // Do not send notify to the same client.
		}

		err := cl.Notify(ctx, notify)
		if err != nil {
			log.Println("can't send notify to:", remoteAddr)
		}
	}

	s.mu.RUnlock()

	return &model.HelloReqResp{Name: "Server"}, nil
}
