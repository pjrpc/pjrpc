package main

import (
	"context"
	"log"
	"time"

	"nhooyr.io/websocket"
)

// It is just custom websocket client.
func main() {
	ctx := context.Background()

	conn, _, err := websocket.Dial(context.Background(), "ws://127.0.0.1:8081/ws", nil)
	if err != nil {
		log.Fatal("websocket.Dial:", err)
	}

	defer conn.Close(websocket.StatusNormalClosure, "bye bye")
	go listen(ctx, conn)

	msg := `{"jsonrpc":"2.0","id":"1","method":"hello","params":{"name":"Client"}}`

	for {
		time.Sleep(time.Second * 3)

		err = conn.Write(ctx, websocket.MessageText, []byte(msg))
		if err != nil {
			log.Fatal("websocket.Send:", err)
		}
	}
}

func listen(ctx context.Context, conn *websocket.Conn) {
	for {
		mt, msg, err := conn.Read(ctx)
		if err != nil {
			log.Fatal("conn.Read:", err)
		}

		log.Println("got message:", mt.String(), ":", string(msg))
	}
}
