package pjrpc_test

import (
	"bytes"
	"encoding/json"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/storage"
)

type dataType struct {
	Data string
}

func (dt dataType) MarshalJSON() ([]byte, error) {
	return []byte(`"` + dt.Data + `"`), nil
}

// TestErrorString testing of the error string formatter.
func TestErrorString(t *testing.T) {
	t.Parallel()

	e := &pjrpc.ErrorResponse{
		Code:    1234,
		Message: "Custom Error",
		Data:    nil,
	}

	assert := func(want string) {
		got := e.Error()
		if want != got {
			t.Fatalf("wrong result:\n%s\n%s", want, got)
		}
	}

	assert("JSON-RPC Error: [1234] Custom Error")

	e.Data = json.RawMessage(`"Some text"`)
	assert(`JSON-RPC Error: [1234] Custom Error ("Some text")`)

	structData := struct {
		Data string
	}{
		Data: "Some text",
	}

	var err error
	if e.Data, err = json.Marshal(structData); err != nil {
		t.Fatal(err)
	}

	assert(`JSON-RPC Error: [1234] Custom Error ({"Data":"Some text"})`)

	if e.Data, err = json.Marshal(dataType{Data: "Some text"}); err != nil {
		t.Fatal(err)
	}

	assert(`JSON-RPC Error: [1234] Custom Error ("Some text")`)

	e = nil
	assert("<nil>")
}

func TestRequestID(t *testing.T) {
	t.Parallel()

	testCase := func(body string, wantID string) {
		t.Helper()

		req := new(pjrpc.Request)

		if err := json.Unmarshal([]byte(body), req); err != nil {
			t.Fatal("failed json.Unmarshal:", err)
		}

		if req.JSONRPC != pjrpc.JSONRPCVersion {
			t.Fatal("wrong version:", req.JSONRPC)
		}

		if req.Method != "method" {
			t.Fatal("not 'method':", req.Method)
		}

		if req.Params == nil {
			t.Fatal("params is nil")
		}

		gotID := req.GetID()
		if wantID != gotID {
			t.Fatal("wrong id:", gotID)
		}
	}

	testCase(`{"jsonrpc":"2.0","method":"method","params":{},"id":123}`, "123")
	testCase(`{"jsonrpc":"2.0","method":"method","params":{},"id":"123"}`, "123")
	testCase(`{"jsonrpc":"2.0","method":"method","params":{},"id":""}`, "")
	testCase(`{"jsonrpc":"2.0","method":"method","params":{}}`, "")
}

func TestResponseID(t *testing.T) {
	t.Parallel()

	testCase := func(wantID string, wantBody string) {
		t.Helper()

		jsonID, err := json.Marshal(wantID)
		if err != nil {
			t.Fatal("failed to json.Marshal id:", err)
		}

		if wantID == "" {
			jsonID = nil
		}

		resp := &pjrpc.Response{
			JSONRPC: pjrpc.JSONRPCVersion,
			ID:      jsonID,
		}

		body, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("failed to json.Marshal resp:", err)
		}

		if wantBody != string(body) {
			t.Fatalf("wrong body: %q", body)
		}

		gotID := resp.GetID()
		if wantID != gotID {
			t.Fatalf("wrong id: %q", gotID)
		}
	}

	testCase("1", `{"jsonrpc":"2.0","id":"1"}`)
	testCase("some-text", `{"jsonrpc":"2.0","id":"some-text"}`)
	testCase("", `{"jsonrpc":"2.0"}`)
}

func TestResponse_SetError(t *testing.T) {
	t.Parallel()

	testCase := func(err error, want string) {
		t.Helper()

		resp := &pjrpc.Response{Result: []byte("some bytes")}
		resp.SetError(err)

		if resp.Result != nil {
			t.Fatal("result not empty: ", string(resp.Result))
		}

		got, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("json.Marshal: ", err)
		}

		if want != string(got) {
			t.Fatalf("wrong result:\n%s\n%s", want, got)
		}
	}

	testCase(
		pjrpc.JRPCErrInvalidParams("field_name"),
		`{"jsonrpc":"","error":{"code":-32602,"message":"Invalid params","data":"field_name"}}`,
	)

	testCase(
		storage.ErrRouteNotFound,
		`{"jsonrpc":"","error":{"code":-32601,"message":"Method not found"}}`,
	)

	testCase(
		pjrpc.ErrBadStatusCode,
		`{"jsonrpc":"","error":{"code":-32000,"message":"Server error","data":"bad status code"}}`,
	)
}

func TestResponse_SetResult(t *testing.T) {
	t.Parallel()

	testCase := func(result any, want string) {
		t.Helper()

		resp := new(pjrpc.Response)
		resp.SetResult(result)

		got, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("json.Marshal: ", err)
		}

		if want != string(got) {
			t.Fatalf("wrong result:\n%s\n%s", want, got)
		}
	}

	testCase(
		"some data",
		`{"jsonrpc":"","result":"some data"}`,
	)

	testCase(
		pjrpc.JRPCErrInternalError(),
		`{"jsonrpc":"","result":{"code":-32603,"message":"Internal error"}}`,
	)

	testCase(
		make(chan int),
		`{"jsonrpc":"","error":{"code":-32603,"message":"Internal error","data":"failed to marshal response"}}`,
	)
}

func TestResponse_JSON(t *testing.T) {
	t.Parallel()

	resp := &pjrpc.Response{
		JSONRPC: pjrpc.JSONRPCVersion,
		ID:      []byte(`"id"`),
		Result:  []byte(`"result"`),
		Error:   nil,
	}

	testCase := func(want string) {
		t.Helper()

		got := resp.JSON()
		if want != string(got) {
			t.Fatal(string(got))
		}
	}

	testCase(`{"jsonrpc":"2.0","id":"id","result":"result"}`)

	resp.SetError(pjrpc.JRPCErrInvalidParams("some error"))
	testCase(`{"jsonrpc":"2.0","id":"id","error":{"code":-32602,"message":"Invalid params","data":"some error"}}`)
}

func TestRequest_JSON(t *testing.T) {
	t.Parallel()

	req := pjrpc.Request{
		JSONRPC: pjrpc.JSONRPCVersion,
		ID:      []byte(`"id"`),
		Method:  "method",
		Params:  []byte(`"string as param"`),
	}

	got := req.JSON()
	want := `{"jsonrpc":"2.0","id":"id","method":"method","params":"string as param"}`

	if want != string(got) {
		t.Fatal(string(got))
	}
}

func TestNewRequest(t *testing.T) {
	t.Parallel()

	req, err := pjrpc.NewRequest("id", "method", "string as param")
	if err != nil {
		t.Fatal(err)
	}

	got := req.JSON()
	want := `{"jsonrpc":"2.0","id":"id","method":"method","params":"string as param"}`

	if want != string(got) {
		t.Fatal(string(got))
	}

	_, err = pjrpc.NewRequest("id", "method", make(chan string))
	if err == nil {
		t.Fatal("wrong type of parameter")
	}
}

func TestNewResponseFrom(t *testing.T) {
	t.Parallel()

	respAssert := func(got, want *pjrpc.Response) {
		t.Helper()

		if got.JSONRPC != want.JSONRPC {
			t.Fatalf("version: %q != %q", got.JSONRPC, want.JSONRPC)
		}

		if got.GetID() != want.GetID() {
			t.Fatalf("id: %q != %q", got.GetID(), want.GetID())
		}

		if !bytes.Equal(got.Result, want.Result) {
			t.Fatalf("result: %q != %q", string(got.Result), string(want.Result))
		}

		if got.Error.Error() != want.Error.Error() {
			t.Fatalf("error: %q != %q", got.Error.Error(), want.Error.Error())
		}
	}

	msg := []byte(`{"jsonrpc":"2.0","id":"id","result":"string as result"}`)
	reader := bytes.NewReader(msg)

	id := []byte(`"id"`)
	result := []byte(`"string as result"`)

	got, err := pjrpc.NewResponseFromJSON(msg)
	if err != nil {
		t.Fatal(err)
	}

	respAssert(got, &pjrpc.Response{JSONRPC: pjrpc.JSONRPCVersion, ID: id, Result: result})

	got, err = pjrpc.NewResponseFromJSONReader(reader)
	if err != nil {
		t.Fatal(err)
	}

	respAssert(got, &pjrpc.Response{JSONRPC: pjrpc.JSONRPCVersion, ID: id, Result: result})

	msg = []byte(`{"jsonrpc":"2.0","id":"id","result":{"field_without_value"}}`)
	reader = bytes.NewReader(msg)

	if _, err = pjrpc.NewResponseFromJSON(msg); err == nil {
		t.Fatal("no error")
	}

	if _, err = pjrpc.NewResponseFromJSONReader(reader); err == nil {
		t.Fatal("no error")
	}

	got = pjrpc.NewResponseFromError(pjrpc.ErrWrongContentType)
	respAssert(got, &pjrpc.Response{
		JSONRPC: pjrpc.JSONRPCVersion,
		Error:   pjrpc.JRPCErrServerError(-32000, pjrpc.ErrWrongContentType.Error()),
	})

	got = pjrpc.NewResponseFromRequest(&pjrpc.Request{ID: id})
	respAssert(got, &pjrpc.Response{JSONRPC: pjrpc.JSONRPCVersion, ID: id})
}

func TestResponse_UnmarshalResult(t *testing.T) {
	t.Parallel()

	resp := &pjrpc.Response{}

	err := resp.UnmarshalResult(nil)
	if err != nil {
		t.Fatal(err)
	}

	resp.Result = []byte(`"string"`)
	var dst string

	err = resp.UnmarshalResult(&dst)
	if err != nil {
		t.Fatal(err)
	}

	if dst != "string" {
		t.Fatal(dst)
	}

	var wrongDst int
	err = resp.UnmarshalResult(&wrongDst)
	if err == nil {
		t.Fatal("no error")
	}
}
