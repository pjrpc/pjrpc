package pjrpc_test

import (
	"context"
	"net"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
)

func TestContext(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	data, ok := pjrpc.ContextGetData(ctx)
	if ok {
		t.Fatal("must be empty")
	}

	if data != nil {
		t.Fatal("must be empty")
	}

	data = &pjrpc.ContextData{}

	ctx = pjrpc.ContextSetData(ctx, data)

	got, ok := pjrpc.ContextGetData(ctx)
	if !ok {
		t.Fatal("must be not empty")
	}

	if data != got {
		t.Fatal("must be equal")
	}

	conn, ok := pjrpc.ContextGetConnection(ctx)
	if ok {
		t.Fatal("must be empty")
	}

	if conn != nil {
		t.Fatal("must be empty")
	}

	conn = &net.TCPConn{}

	ctx = pjrpc.ContextSetConnection(ctx, conn)

	gotConn, ok := pjrpc.ContextGetConnection(ctx)
	if !ok {
		t.Fatal("must be not empty")
	}

	if conn != gotConn {
		t.Fatal("must be equal")
	}
}
