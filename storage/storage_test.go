package storage_test

import (
	"errors"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2/storage"
)

type handler func(route string)

// TestStorage tests that storage can put and return values.
func TestStorage(t *testing.T) {
	t.Parallel()

	assertNoError := func(err error) {
		t.Helper()
		if err != nil {
			t.Fatal(err)
		}
	}

	r1 := "route1"
	var h1 handler = func(route string) {
		t.Helper()
		if route != r1 {
			t.Fatalf("wrong route in h1:%q != %q", r1, route)
		}
	}

	r2 := "route2"
	var h2 handler = func(route string) {
		t.Helper()
		if route != r2 {
			t.Fatalf("wrong route in h2:%q != %q", r2, route)
		}
	}

	s := storage.New()

	s.Put(r1, h1)
	h, err := s.Get(r1)
	assertNoError(err)
	h.(handler)(r1)

	h, err = s.Get(r2)
	if !errors.Is(err, storage.ErrRouteNotFound) {
		t.Fatal(err)
	}
	if h != nil {
		t.Fatal(h)
	}

	s.Put(r1, h2) // Handler 2 with Route 1.
	s.Put(r2, h2)

	h, err = s.Get(r2)
	assertNoError(err)
	h.(handler)(r2)
	h, err = s.Get(r1)
	assertNoError(err)
	h.(handler)(r2) // Route 1, must work with Route 2.
}
