package pjrpc_test

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net"
	"strings"
	"testing"
	"time"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

type acceptData struct {
	conn net.Conn
	err  error
}

// testMockListener implements methods of the net.Listener.
type testMockListener struct {
	accept chan acceptData
}

func (tl *testMockListener) Accept() (net.Conn, error) {
	data := <-tl.accept
	return data.conn, data.err
}

func (tl *testMockListener) Close() error   { return nil }
func (tl *testMockListener) Addr() net.Addr { return nil }

func newTestMockListener() *testMockListener {
	return &testMockListener{
		accept: make(chan acceptData, 1),
	}
}

type rwData struct {
	data string
	err  error
}

type testMockConn struct {
	t *testing.T

	read  chan rwData
	write chan rwData

	needWriteDone bool
	writeDone     chan struct{}
}

func (tc *testMockConn) Read(b []byte) (n int, err error) {
	d := <-tc.read

	if d.err != nil {
		return 0, d.err
	}

	return strings.NewReader(d.data).Read(b)
}

func (tc *testMockConn) Write(b []byte) (n int, err error) {
	if tc.needWriteDone {
		defer func() {
			tc.writeDone <- struct{}{}
		}()
	}

	d := <-tc.write

	if d.data != string(b) {
		tc.t.Fatalf("wrong data in writer:\n%s\n%s", d.data, b)
	}

	return len(d.data), d.err
}

func (tc *testMockConn) Close() error                       { return nil }
func (tc *testMockConn) LocalAddr() net.Addr                { return nil }
func (tc *testMockConn) RemoteAddr() net.Addr               { return nil }
func (tc *testMockConn) SetDeadline(t time.Time) error      { return nil }
func (tc *testMockConn) SetReadDeadline(t time.Time) error  { return nil }
func (tc *testMockConn) SetWriteDeadline(t time.Time) error { return nil }

func newTestMockConn(t *testing.T) *testMockConn {
	t.Helper()

	return &testMockConn{
		t:         t,
		read:      make(chan rwData, 1),
		write:     make(chan rwData, 1),
		writeDone: make(chan struct{}, 1),
	}
}

func TestServerListener_Listen(t *testing.T) {
	t.Parallel()

	listener := newTestMockListener()
	srv := pjrpc.NewServerListener(listener)

	srvErrors := make(chan error, 1)

	go func() {
		srvErrors <- srv.Listen()
	}()

	// Closed listiner imitation.
	err := srv.Close()
	if err != nil {
		t.Fatal(err)
	}

	listener.accept <- acceptData{err: net.ErrClosed}

	err = <-srvErrors
	if err != nil {
		t.Fatal(err)
	}

	// Unexpected error with default handler.
	go func() {
		srvErrors <- srv.Listen()
	}()

	listener.accept <- acceptData{err: net.ErrWriteToConnected}

	err = <-srvErrors
	if !errors.Is(err, net.ErrWriteToConnected) {
		t.Fatal(err)
	}

	// The same but handler igonre errors.
	srv.OnErrorAccept = func(err error) error {
		if errors.Is(err, net.ErrWriteToConnected) {
			return nil
		}

		return err
	}

	go func() {
		srvErrors <- srv.Listen()
	}()

	listener.accept <- acceptData{err: net.ErrWriteToConnected}
	listener.accept <- acceptData{err: io.EOF}

	err = <-srvErrors
	if !errors.Is(err, io.EOF) {
		t.Fatal(err)
	}

	// Check Handler method with custom OnNewConnection method.
	gotConnection := make(chan struct{})

	srv.OnNewConnection = func(_ context.Context, conn net.Conn) bool {
		t.Helper()

		if _, ok := conn.(*testMockConn); !ok {
			t.Fatal("wrong connection")
		}

		close(gotConnection)

		return false
	}

	go func() {
		srvErrors <- srv.Listen()
	}()

	listener.accept <- acceptData{conn: newTestMockConn(t)}

	<-gotConnection

	listener.accept <- acceptData{err: net.ErrClosed}

	err = <-srvErrors
	if err != nil {
		t.Fatal(err)
	}
}

func TestServerListener_Handler_Positive(t *testing.T) {
	t.Parallel()

	listener := pjrpc.NewServerListener(nil)

	tc := newTestMockConn(t)
	handlerDone := make(chan struct{})

	go func() {
		listener.Handler(context.Background(), tc)
		close(handlerDone)
	}()

	// Regular method.
	listener.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (any, error) {
		if string(params) != `"params"` {
			return "wrong", nil
		}

		return "result", nil
	})

	tc.needWriteDone = true
	tc.read <- rwData{data: `{"jsonrpc":"2.0","id":"1","method":"method","params":"params"}`}
	tc.write <- rwData{data: `{"jsonrpc":"2.0","id":"1","result":"result"}`}

	<-tc.writeDone

	// Regular notification.
	notifyDone := make(chan struct{})

	listener.RegisterMethod("notify", func(ctx context.Context, params json.RawMessage) (any, error) {
		close(notifyDone)
		return nil, nil
	})

	tc.needWriteDone = false
	tc.read <- rwData{data: `{"jsonrpc":"2.0","method":"notify","params":"params"}`}

	<-notifyDone

	// Connection is closed.
	tc.read <- rwData{err: io.EOF}
	<-handlerDone
}

func TestServerListener_Handler_Negative(t *testing.T) {
	t.Parallel()

	type testData struct {
		tc          *testMockConn
		handlerDone chan struct{}
		listener    *pjrpc.ServerListener
	}

	testCase := func() *testData {
		td := &testData{
			tc:          newTestMockConn(t),
			handlerDone: make(chan struct{}),
			listener:    pjrpc.NewServerListener(nil),
		}

		go func() {
			td.listener.Handler(context.Background(), td.tc)
			close(td.handlerDone)
		}()

		return td
	}

	const (
		readData  = `{"jsonrpc":"2.0","id":1,"method":"method","params":"params"}`
		writeData = `{"jsonrpc":"2.0","id":1,"error":{"code":-32601,"message":"Method not found"}}`
	)

	// Default behavior on unexpected errors.
	// Can't read.
	td := testCase()

	closedHandler := make(chan struct{})
	td.listener.OnCloseConnection = func(context.Context, net.Conn) { close(closedHandler) }

	td.tc.read <- rwData{err: io.ErrUnexpectedEOF}

	<-td.handlerDone
	<-closedHandler

	// Can't write.
	td = testCase()

	td.tc.read <- rwData{data: readData}
	td.tc.write <- rwData{data: writeData, err: io.ErrNoProgress}

	<-td.handlerDone

	// Custom error handlers.
	td = testCase()

	td.listener.OnErrorReceive = func(context.Context, net.Conn, error) (needClose bool) {
		return false
	}

	td.listener.OnErrorSend = func(_ context.Context, _ net.Conn, err error) (needClose bool) {
		return !errors.Is(err, io.ErrNoProgress) // Ignore of the ErrNoProgress.
	}

	td.tc.read <- rwData{err: io.ErrUnexpectedEOF} // Listener will ignore this error.

	// Listener's reader is recovered.
	td.tc.read <- rwData{data: readData}                          // No read error here.
	td.tc.write <- rwData{data: writeData, err: io.ErrNoProgress} // Listener will igonre this error.

	// Listener's writer is recovered.
	td.tc.read <- rwData{data: readData}   // No read error here.
	td.tc.write <- rwData{data: writeData} // No write error here.

	// Listener works stable.
	td.tc.read <- rwData{data: readData}                             // No read error here.
	td.tc.write <- rwData{data: writeData, err: io.ErrUnexpectedEOF} // Critical error here.

	// Critical error killed the listener.
	<-td.handlerDone
}

func TestServerListener_TCPE2E(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	const (
		address    = "localhost:37353"
		wantResult = "result"
	)

	listener, err := net.Listen("tcp", address)
	if err != nil {
		t.Fatal(err)
	}

	srv := pjrpc.NewServerListener(listener)

	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (any, error) {
		return wantResult, nil
	})

	srvErr := make(chan error)
	go func() {
		srvErr <- srv.Listen()
	}()

	conn, err := net.Dial("tcp", address)
	if err != nil {
		t.Fatal(err)
	}

	cl := client.NewAsyncClient(conn)

	clErr := make(chan error)
	go func() {
		clErr <- cl.Listen()
	}()

	var result string
	err = cl.Invoke(ctx, "1", "method", "params", &result)
	if err != nil {
		t.Fatal(err)
	}

	if result != wantResult {
		t.Fatal(result)
	}

	if err = cl.Close(); err != nil {
		t.Fatal(err)
	}

	if err = srv.Close(); err != nil {
		t.Fatal(err)
	}

	if err = <-clErr; err != nil {
		t.Fatal(err)
	}

	if err = <-srvErr; err != nil {
		t.Fatal(err)
	}
}
