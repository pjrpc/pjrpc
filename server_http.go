package pjrpc

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
)

var (
	_ http.Handler = &ServerHTTP{}
	_ Registrator  = &ServerHTTP{}
)

// ServerHTTP JSON-RPC server over HTTP protocol.
type ServerHTTP struct {
	*Server

	MaxBodyLimit int64
}

// ServerHTTPOption options to modify parameters of the HTTP server.
type ServerHTTPOption func(s *ServerHTTP)

// ServerHTTPOptionMaxBodyLimit sets limit of body reader.
func ServerHTTPOptionMaxBodyLimit(limit int64) ServerHTTPOption {
	return func(s *ServerHTTP) { s.MaxBodyLimit = limit }
}

// NewServerHTTP creates new server with default error handlers and empty router.
func NewServerHTTP(options ...ServerHTTPOption) *ServerHTTP {
	s := &ServerHTTP{Server: NewServer()}

	for _, option := range options {
		option(s)
	}

	return s
}

// ServeHTTP implements interface of handler in http package.
func (s *ServerHTTP) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if !strings.Contains(r.Header.Get(ContentTypeHeaderName), ContentTypeHeaderValue) {
		s.sendMessage(w, NewResponseFromError(JRPCErrInvalidRequest("content-type must be 'application/json'")).JSON())
		return
	}

	var src io.ReadCloser
	if s.MaxBodyLimit != 0 {
		src = http.MaxBytesReader(w, r.Body, s.MaxBodyLimit)
	} else {
		src = r.Body
	}

	body, err := io.ReadAll(src)
	if err != nil {
		s.sendMessage(w, NewResponseFromError(JRPCErrParseError("failed to read body: "+err.Error())).JSON())
		return
	}

	ctx := ContextSetData(r.Context(), &ContextData{HTTPRequest: r})

	resp := s.Serve(ctx, body)

	s.sendMessage(w, resp)
}

func (s *ServerHTTP) sendMessage(w http.ResponseWriter, body json.RawMessage) {
	w.Header().Set(ContentTypeHeaderName, ContentTypeHeaderValue)
	w.WriteHeader(http.StatusOK)

	_, _ = w.Write(body) //nolint:errcheck // It's client side problem don't care about it.
}
