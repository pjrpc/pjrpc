// Package analyzer scanning source code and returns result of the scanning.
// Searches interfaces as a rpc description and returns list of the methods.
package analyzer

import (
	"context"
	"fmt"
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/so_literate/gentools/packager"
	"golang.org/x/tools/go/packages"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

// Analyzer contains method to scan source code.
type Analyzer struct {
	log  zerolog.Logger
	conf *config.Search
	pkgr *packager.Packager
}

// New creates new analyzer with search configuration.
func New(log *zerolog.Logger, conf *config.Search) (*Analyzer, error) {
	pkgr, err := packager.New()
	if err != nil {
		return nil, fmt.Errorf("packager.New: %w", err)
	}

	a := &Analyzer{
		log:  log.With().Bool("analyzer", true).Logger(),
		conf: conf,
		pkgr: pkgr,
	}

	return a, nil
}

// Run runs analyzer over source code and returns result of the scanning.
func (a *Analyzer) Run(ctx context.Context) (*model.Service, error) {
	a.log.Debug().Interface("conf", *a.conf).Msg("analyzer running")

	kntps, err := ParseKnownTypes(a.conf.KnownTypesPath)
	if err != nil {
		return nil, fmt.Errorf("failed to parse known types file: %w", err)
	}

	mergeKnownTypes(kntps, knownTypes)

	for _, pat := range a.pkgr.SkipPatterns {
		a.log.Debug().Str("pattern", pat.String()).Msg("pkgr skip pattern")
	}

	if a.conf.Name == "" {
		return nil, fmt.Errorf("%w: search interface name is empty", ErrInvalidData)
	}

	cfg := &packages.Config{
		Mode:    packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo,
		Context: ctx,
		Dir:     a.conf.Path,
	}

	pkgs, err := a.pkgr.PackagesLoad(cfg)
	if err != nil {
		return nil, fmt.Errorf("packages.Load: %w", err)
	}

	if len(pkgs) != 1 {
		return nil, fmt.Errorf("%w: want 1, got %d", ErrWrongNumberPackages, len(pkgs))
	}

	pkg := pkgs[0]

	if len(pkg.Errors) != 0 {
		texts := make([]string, len(pkg.Errors))
		for i, pkgErr := range pkg.Errors {
			texts[i] = fmt.Sprintf(" - %s", pkgErr.Error())
		}

		return nil, fmt.Errorf("%w:\n%s", ErrGoSyntax, strings.Join(texts, "\n"))
	}

	log := a.log.With().Str("pkgPath", pkg.PkgPath).Logger()

	log.Debug().Msg("start analyzing the package")

	service, err := a.scanPackage(pkg)
	if err != nil {
		return nil, fmt.Errorf("scanPackage %q: %w", pkg.PkgPath, err)
	}

	log.Debug().Msg("analyzing done")

	return service, nil
}
