package printer_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/printer"
)

func TestExtractImportPathFromPath(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	const rpcModelPath = "host.com/user/repo/model/rpcmodel"

	testCase := func(absPath, importPath, wantImport, wantPkg string) {
		t.Helper()

		gotImport, gotPkg := printer.ExtractImportPathFromPath(absPath, importPath)
		so.Equal(wantImport, gotImport)
		so.Equal(wantPkg, gotPkg)
	}

	testCase(
		"/abs/path/to/code/host.com/user/repo/server/model",
		rpcModelPath,

		"host.com/user/repo/server/model",
		"model",
	)

	testCase(
		"/abs/path/to/code/host.com/user/repo/model/rpcmodel/server",
		rpcModelPath,

		"host.com/user/repo/model/rpcmodel/server",
		"server",
	)

	testCase(
		"/abs/path/to/code/host.com/user/repo/model/rpcmodel",
		rpcModelPath,

		"host.com/user/repo/model/rpcmodel",
		"rpcmodel",
	)

	testCase(
		"/abs/path/to/code/repo/model/rpcmodel",
		rpcModelPath,

		"host.com/user/repo/model/rpcmodel",
		"rpcmodel",
	)

	testCase(
		"/abs/path/to/code/nothing/common/between/them",
		rpcModelPath,

		"",
		"",
	)
}

func TestSetStringIfEmpty(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	field := ""
	text := "some text"

	printer.SetStringIfEmpty(&field, text)
	so.Equal(text, field)

	printer.SetStringIfEmpty(&field, "Another text")
	so.Equal(text, field)
}

func TestGetFirstLine(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	testCase := func(text string, want string) {
		t.Helper()

		got := printer.GetFirstLine(text)
		so.Equal(want, got)
	}

	testCase("First line\nSecond Line\n", "First line")
	testCase("First line\n", "First line")
	testCase("First line", "First line")
	testCase("", "")
}
