module gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/shopspring/decimal v1.3.1
	gitlab.com/pjrpc/pjrpc/v2 v2.1.0
)
