package client

import (
	"sync"

	"gitlab.com/pjrpc/pjrpc/v2"
)

// AsyncResult is response channel for client.
type AsyncResult chan *pjrpc.Response

// AsyncResults is a map with mutex.
// Provides a storage of expected responses for async client.
type AsyncResults struct {
	mu   sync.Mutex
	data map[string]AsyncResult
}

// NewAsyncResults returns new initiated storage.
func NewAsyncResults() *AsyncResults {
	return &AsyncResults{
		data: make(map[string]AsyncResult),
	}
}

// CreateResult creates new result channel and stores it with id.
func (r *AsyncResults) CreateResult(id string) AsyncResult {
	res := make(AsyncResult, 1)

	r.mu.Lock()
	r.data[id] = res
	r.mu.Unlock()

	return res
}

// SendResult send result into channel by id and removes channel from storage.
func (r *AsyncResults) SendResult(id string, resp *pjrpc.Response) bool {
	r.mu.Lock()

	res, ok := r.data[id]
	if !ok {
		r.mu.Unlock()
		return false
	}

	delete(r.data, id)
	r.mu.Unlock()

	res <- resp
	close(res)

	return true
}

// DeleteResult deletes result channel from storage.
func (r *AsyncResults) DeleteResult(id string) {
	r.mu.Lock()
	delete(r.data, id)
	r.mu.Unlock()
}
