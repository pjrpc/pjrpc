// Package ws contains JSON-RPC server over Websocket protocol.
package ws

import (
	"context"
	"fmt"
	"net/http"

	"nhooyr.io/websocket"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

var (
	_ http.Handler      = &ServerWebsocket{}
	_ pjrpc.Registrator = &ServerWebsocket{}
)

// Config is optional parameter to create websocket server.
// AcceptOptions is required field, feel free to set nil on others.
type Config struct {
	// Optional config of the Websocket accepter.
	AcceptOptions *websocket.AcceptOptions

	// Accept is the first HTTP handler.
	// You can refuse connection here.
	Accept func(w http.ResponseWriter, r *http.Request) (accepted bool)

	// OnErrorWebsocketAccept optional handler to debug failed websocket connect accept.
	OnErrorWebsocketAccept func(r *http.Request, err error)
}

// ServerWebsocket JSON-RPC server over WebSocket protocol.
type ServerWebsocket struct {
	*pjrpc.ServerListener

	accept func(w http.ResponseWriter, r *http.Request)
}

// NewServerWebsocket returns new Websocket Server based on your config.
func NewServerWebsocket(config *Config) *ServerWebsocket {
	if config == nil {
		config = &Config{}
	}

	s := &ServerWebsocket{
		ServerListener: pjrpc.NewServerListener(nil),
	}

	s.accept = func(w http.ResponseWriter, r *http.Request) {
		if config.Accept != nil && !config.Accept(w, r) {
			return
		}

		c, err := websocket.Accept(w, r, config.AcceptOptions)
		if err != nil {
			if config.OnErrorWebsocketAccept != nil {
				config.OnErrorWebsocketAccept(r, err)
			}

			return
		}

		ctx := r.Context()
		data := &pjrpc.ContextData{HTTPRequest: r}

		conn := websocket.NetConn(ctx, c, websocket.MessageText)

		ctx = pjrpc.ContextSetConnection(ctx, conn)
		ctx = pjrpc.ContextSetData(ctx, data)

		r = r.WithContext(ctx)
		data.HTTPRequest = r

		s.Handler(ctx, conn)
	}

	return s
}

// ServeHTTP implements the http.Handler interface for a WebSocket server.
func (s *ServerWebsocket) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.accept(w, r)
}

// NewClient returns new async client based on websocket connection.
func NewClient(ctx context.Context, endpoint string, options *websocket.DialOptions) (*client.AsyncClient, error) {
	c, _, err := websocket.Dial(ctx, endpoint, options) //nolint:bodyclose // response body is closed.
	if err != nil {
		return nil, fmt.Errorf("websocket.Dial: %w", err)
	}

	return client.NewAsyncClient(websocket.NetConn(ctx, c, websocket.MessageText)), nil
}
