module gitlab.com/pjrpc/pjrpc/ws

go 1.22

require (
	gitlab.com/pjrpc/pjrpc/v2 v2.5.1
	nhooyr.io/websocket v1.8.10
)

replace gitlab.com/pjrpc/pjrpc/v2 => ../
