package ws_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"nhooyr.io/websocket"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/ws"
)

type params struct {
	Name string
}

//nolint:gocognit // little complex test because a lot of checks in the rpc handler.
func TestServerWebsocket(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	mux := http.NewServeMux()
	httpServer := httptest.NewServer(mux)

	endpoint := fmt.Sprintf("ws://%s/ws", httpServer.Listener.Addr())

	gotConnect := make(chan struct{})
	var gotRequest *http.Request

	conf := &ws.Config{
		AcceptOptions: &websocket.AcceptOptions{
			OriginPatterns: []string{"test-domain.com"},
		},
		Accept: func(w http.ResponseWriter, r *http.Request) (accepted bool) {
			gotRequest = r
			close(gotConnect)
			return true
		},
	}

	wsServer := ws.NewServerWebsocket(conf)

	var wantParams []byte
	count := 0

	wsServer.RegisterMethod("test", func(ctx context.Context, gotParams json.RawMessage) (any, error) {
		if !bytes.Equal(wantParams, gotParams) {
			t.Fatalf("got wrong params:\nwant: %s\ngot:  %s", wantParams, gotParams)
		}

		data, ok := pjrpc.ContextGetData(ctx)
		if !ok {
			t.Fatal("no JSON-RPC context")
		}

		_, ok = pjrpc.ContextGetConnection(ctx)
		if !ok {
			t.Fatal("no connection in context")
		}

		if reflect.DeepEqual(data.HTTPRequest, gotRequest) {
			t.Fatal("request should be different in deep")
		}

		deepAssert(t, data.HTTPRequest.URL, gotRequest.URL)
		deepAssert(t, data.HTTPRequest.Header, gotRequest.Header)
		deepAssert(t, data.HTTPRequest.Method, gotRequest.Method)
		deepAssert(t, data.HTTPRequest.ContentLength, gotRequest.ContentLength)
		deepAssert(t, data.HTTPRequest.Host, gotRequest.Host)

		if data.JRPCRequest.Method != "test" {
			t.Fatal("wrong method:", data.JRPCRequest.Method)
		}

		count++

		return params{Name: fmt.Sprintf("response %d", count)}, nil
	})

	mux.Handle("/ws", wsServer)

	dialOptions := &websocket.DialOptions{HTTPHeader: http.Header{"Origin": []string{"http://test-domain.com"}}}

	wsClient, err := ws.NewClient(context.Background(), endpoint, dialOptions)
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		_ = wsClient.Listen()
		wsClient.Close()
	}()

	<-gotConnect

	var res params

	wantParams = []byte(`{"Name":"first request"}`)
	err = wsClient.Invoke(ctx, "1", "test", params{Name: "first request"}, &res)
	if err != nil {
		t.Fatal(err)
	}

	if res.Name != "response 1" {
		t.Fatal("got wrong response:", res.Name)
	}

	wantParams = []byte(`{"Name":"2nd request"}`)
	err = wsClient.Invoke(ctx, "2", "test", params{Name: "2nd request"}, &res)
	if err != nil {
		t.Fatal(err)
	}

	if res.Name != "response 2" {
		t.Fatal("got wrong response:", res.Name)
	}
}

func TestNewClient(t *testing.T) {
	t.Parallel()

	_, err := ws.NewClient(context.Background(), "ws://localhost/ws", nil)
	if err == nil {
		t.Fatal("must be error")
	}
}

func deepAssert(t *testing.T, want, got any) {
	t.Helper()

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("values are not equal:\nwant: %#v\ngot:  %#v", want, got)
	}
}
