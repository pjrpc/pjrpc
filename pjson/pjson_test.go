package pjson_test

import (
	"bytes"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2/pjson"
)

// Just smoke tests coverage mining.

func TestDecoding(t *testing.T) {
	t.Parallel()

	msg := []byte(`{"field":"value"}`)
	r := bytes.NewReader(msg)

	var res map[string]string

	err := pjson.NewDecoder(r).Decode(&res)
	if err != nil {
		t.Fatal(err)
	}

	if res["field"] != "value" {
		t.Fatal(res)
	}

	res = map[string]string{}

	err = pjson.Unmarshal(msg, &res)
	if err != nil {
		t.Fatal(err)
	}

	if res["field"] != "value" {
		t.Fatal(res)
	}
}

func TestEncoding(t *testing.T) {
	t.Parallel()

	want := `{"field":"value"}`
	data := map[string]string{"field": "value"}
	dst := bytes.NewBuffer(nil)

	err := pjson.NewEncoder(dst).Encode(data)
	if err != nil {
		t.Fatal(err)
	}

	if dst.String() != want+"\n" {
		t.Fatal(dst.String())
	}

	got, err := pjson.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	if string(got) != want {
		t.Fatal(string(got))
	}
}
