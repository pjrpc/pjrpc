package pjrpc_test

import (
	"bytes"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
)

func assertErrorResponse(t *testing.T, gotError *pjrpc.ErrorResponse, wantCode int, wantMessage string, wantData []byte) {
	t.Helper()

	if gotError == nil {
		t.Fatal("empty error")
	}

	if wantCode != gotError.Code {
		t.Fatal("wrong error code:", gotError.Code)
	}

	if wantMessage != gotError.Message {
		t.Fatal("wrong message:", gotError.Message)
	}

	if !bytes.Equal(wantData, gotError.Data) {
		t.Fatal("wrong data:", string(wantData))
	}
}

// TestNewJRPCErrServerError testing of the creation new JSON-RPC error.
func TestNewJRPCErrServerError(t *testing.T) {
	t.Parallel()

	assertErrorResponse(
		t,
		pjrpc.JRPCErrServerError(100),
		100,
		"Server error",
		nil,
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrServerError(101, nil),
		101,
		"Server error",
		[]byte(`null`),
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrServerError(-32000, "Some text"),
		-32000,
		"Server error",
		[]byte(`"Some text"`),
	)

	var didPanic bool

	test := func() {
		t.Helper()

		defer func() {
			if rec := recover(); rec != nil {
				didPanic = true
			}
		}()

		if err := pjrpc.JRPCErrServerError(123, make(chan struct{})); err != nil {
			t.Fatalf("err is not nil: %#v", err)
		}
	}

	test()
	if !didPanic {
		t.Fatal("not panic")
	}
}

// TestErrors testing error codes.
// https://www.jsonrpc.org/specification#error_object
func TestErrors(t *testing.T) {
	t.Parallel()

	assertErrorResponse(
		t,
		pjrpc.JRPCErrParseError(),
		-32700,
		"Parse error",
		nil,
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrInvalidRequest(),
		-32600,
		"Invalid Request",
		nil,
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrMethodNotFound(),
		-32601,
		"Method not found",
		nil,
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrInvalidParams(),
		-32602,
		"Invalid params",
		nil,
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrInternalError(),
		-32603,
		"Internal error",
		nil,
	)

	assertErrorResponse(
		t,
		pjrpc.JRPCErrInvalidParams("some text"),
		-32602,
		"Invalid params",
		[]byte(`"some text"`),
	)
}
